#coding:UTF-8

import sys
import numpy as np

vertices = []
facets = []
facetsNum = 0

def load(_name):
	try:
		f = open(_name+".obj", "r")
	except IOError:
		print("cannot open file")
		sys.exit()

	while True:
		line = f.readline()
		if not line:
			break

		if processLine(line):
			pass
		else:
			pass
	f.close()

def processLine(line):
	global facetsNum

	parts = line.split(" ")
	if len(parts) == 0:
		return False

	if parts[0] == "v":
		tmp = np.array([float(parts[1]), float(parts[2]), float(parts[3])])
		vertices.append(tmp)
		return True

	if parts[0] == "f":
		facets.append(int(parts[1])-1)
		facets.append(int(parts[2])-1)
		facets.append(int(parts[3])-1)
		facetsNum += 1
		return True

def outputVTK(_name):
	output = open(_name+".vtk", mode="w", encoding="ascii")
	output.write("# vtk DataFile Version 2.0\n")
	output.write("OBJtoVTKmesh\n")
	output.write("ASCII\n")
	output.write("DATASET UNSTRUCTURED_GRID\n")
	output.write("POINTS {0} float\n".format(len(vertices)))

	for i in range(len(vertices)):
		tmp_v = vertices[i]
		output.write("{0} {1} {2}\n".format(tmp_v[0], tmp_v[1], tmp_v[2]))

	output.write("CELLS {0} {1}\n".format(facetsNum, 4*facetsNum))

	for i in range(facetsNum):
		output.write("3 {0} {1} {2}\n".format(facets[3*i+0], facets[3*i+1], facets[3*i+2]))

	output.write("CELL_TYPES {0}\n".format(facetsNum))

	for i in range(facetsNum):
		output.write("5\n")

	output.close()		


if __name__ == '__main__':
	filename = input("ファイルの名前は？（.objは抜いて書いてね）")
	load(filename)
	outputVTK(filename)