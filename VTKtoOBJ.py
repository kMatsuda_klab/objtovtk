#coding:UTF-8

import sys
import numpy as np

vertices = []
facets = []
readerFlag = 0

def load(_name):
	global readerFlag
	try:
		f = open(_name+".vtk", "r")
	except IOError:
		print("cannot open file: " + _name + ".vtk\n")
		return False

	while True:
		line = f.readline()
		if not line:
			break

		if processLine(line):
			pass
		else:
			pass
	f.close()
	readerFlag = 0
	return True

def processLine(line):
	global readerFlag

	parts = line.split(" ")
	if len(parts) == 0:
		return False

	if parts[0] == "POINTS":
		readerFlag = 1
		return True

	if parts[0] == "CELLS":
		readerFlag = 2
		return True

	if parts[0] == "CELL_TYPES":
		readerFlag = 0
		return True

	if readerFlag == 1:
		tmp = np.array([float(parts[0]), float(parts[1]), float(parts[2])])
		vertices.append(tmp)
		return True

	if readerFlag == 2:
		f_parts = line.split(" ")
		print(f_parts)
		v_id = [int(f_parts[1]), int(f_parts[2]), int(f_parts[3])]
		facets.append(v_id)
		return True

def outputOBJ(_name):
	output = open(_name+".obj", mode="w", encoding="ascii")

	for i in range(len(vertices)):
		tmp_v = vertices[i]
		output.write("v {0} {1} {2}\n".format(tmp_v[0], tmp_v[1], tmp_v[2]))

	for i in range(len(facets)):
		tmp_f = facets[i]
		print(tmp_f)
		output.write("f {0} {1} {2}\n".format(tmp_f[0]+1, tmp_f[1]+1, tmp_f[2]+1))

	output.close()

if __name__ == '__main__':
	filename = input("ファイルの名前は？(.vtkは抜いてね): ")
	load(filename)
	outputOBJ(filename)